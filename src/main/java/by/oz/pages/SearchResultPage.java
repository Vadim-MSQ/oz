package by.oz.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SearchResultPage extends BasePage {
    private String item;

    public SearchResultPage(WebDriver driver, String search) {
        super(driver);
        this.item = search;
    }

    @Step("Gather search results into collection")
    public List<String> getSearchResults() {
        List<WebElement> elementList = new ArrayList<WebElement>();
        elementList.addAll(getElemsByCSS(".item-type-card__title"));
        elementList.addAll(getElemsByCSS(".item-type-card__info"));

        List<String> listResults = new ArrayList<String>();
        Iterator<WebElement> iterator = elementList.iterator();
        while (iterator.hasNext()) {
            String elem = iterator.next().getText();
            if (elem.equalsIgnoreCase(item)) {
                listResults.add(elem);
            }
        }
        return listResults;
    }

    @Step("Choose 1st product")
    public ItemPage chooseItem() {
        List<WebElement> list = getElemsByCSS(".item-type-card__link");
        list.get(0).click();
/*        for (WebElement element : list) {
            if (element.getText().equalsIgnoreCase(item)) {
*//*                WebElement parent = (WebElement) ((JavascriptExecutor) driver).executeScript(
                        "return arguments[0].parentNode;", element);
                parent.click();*//*
                element.click();
                break;
            }
        }*/
        waitForPageLoad(driver);
        return new ItemPage(driver);
    }
}
