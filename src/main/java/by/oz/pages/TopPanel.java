package by.oz.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TopPanel extends BasePage {
    public TopPanel(WebDriver driver) {
        super(driver);
    }

    @Step("Go to shopping cart")
    public ShoppingBasketPage goToCart() {
        getElemByCSS(".top-panel__userbar__cart__item").click();
        return new ShoppingBasketPage(driver);
    }

    @Step("Get total quantity of ordered products displaying in top panel")
    public int getNumberOfItems() {
        int result = 0;
        if (isElementPresent(By.xpath("//span[@style=\"display: none;\"]"))) {
            result = 0;
        } else if (isElementPresent(By.cssSelector(".top-panel__userbar__cart__count"))) {
            result = Integer.parseInt((getElemByCSS(".top-panel__userbar__cart__count").getText()));
        }
        return result;
    }

    @Step("Search products with world: {searchPhrase}")
    public SearchResultPage search(String searchPhrase) {
        getElemByCSS("#top-s").sendKeys(searchPhrase);
        getElemByCSS(".top-panel__search__btn").click();
        return new SearchResultPage(driver, searchPhrase);
    }
}
