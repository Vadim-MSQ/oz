package by.oz.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class BasePage {
    public WebDriver driver;
    public WebDriverWait wait;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 5);
    }
    public void waitForPageLoad(WebDriver driver) {
        wait.until(new ExpectedCondition<Boolean>() {
            //@Override
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
            }
        });
    }

    public String getTitle() {
        return driver.getTitle();
    }

    public void waitForElement(By locator) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public WebElement getElemByCSS(String selector){
        waitForElement(By.cssSelector(selector));
        return driver.findElement(By.cssSelector(selector));
    }

    public List<WebElement> getElemsByCSS(String selector){
        waitForElement(By.cssSelector(selector));
        return driver.findElements(By.cssSelector(selector));
    }

    public void waitForElement(WebDriver driver, WebElement element) {
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(30, TimeUnit.SECONDS)
                .pollingEvery(3, TimeUnit.SECONDS)
                .ignoring(java.util.NoSuchElementException.class);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public boolean isElementPresent(By locator) {
        try {
            driver.findElement(locator);
            return true;
        } catch (NoSuchElementException ex) {
            return false;
        }
    }

    @Step("Scroll page down")
    public void scrollPageDown(){
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,500)", "");
//        jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");  //scroll down to page
    }

    @Step("Scroll page down")
    public void scrollPageUp(){
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,-500)", "");
    }
}
