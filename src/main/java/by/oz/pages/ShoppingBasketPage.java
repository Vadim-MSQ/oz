package by.oz.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ShoppingBasketPage extends BasePage {
    public ShoppingBasketPage(WebDriver driver) {
        super(driver);
    }

    @Step("Summarize quantity of ordered products in shopping cart")
    public int getNumberOfItems() {
        int result = 0;
        if(isElementPresent(By.cssSelector(".i-amount-select__key"))) {
            List<WebElement> list = getElemsByCSS(".i-amount-select__key");
            for (WebElement elem : list) {
                result = result + Integer.parseInt(elem.getAttribute("value"));
            }
        }
        return result;
    }

    @Step("Change quantity of ordered product:{item} to:{quantity}")
    public void setQuantityOfItem(String item, int quantity) {
        List<WebElement> elementList = getElemsByCSS(".goods-table-cell__line_title");
        int index = -1;

        for (WebElement element : elementList) {
            index++;
            if (element.getText().equalsIgnoreCase(item)) {
                break;
            }
        }
        driver.findElements(By.cssSelector(".i-amount-select__key_button")).get(index).click();
        waitForElement(By.cssSelector(".i-amount-select__items.i-amount-select__items_open"));
        driver.findElement(By.cssSelector("li.i-amount-select__item:nth-child(" + quantity + ")")).click();
    }

    @Step("Mark all products in shopping cart")
    public void checkAllItems() {
        WebElement checkAll = getElemsByCSS(".i-checkbox__faux")
                .get(getElemsByCSS(".i-checkbox__faux").size() - 1);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click()", checkAll);
    }

    @Step("Delete all product(s) from shopping cart")
    public void delete() {
        waitForElement(By.cssSelector(".i-button.i-button_danger.i-button_small.remove"));
        getElemByCSS(".i-button.i-button_danger.i-button_small.remove").click();
        waitForElement(By.cssSelector(".i-button.i-button_danger.i-button_small.remove-yes"));
        if (isElementPresent(By.cssSelector(".i-button.i-button_danger.i-button_small.remove-yes"))) {
            getElemByCSS(".i-button.i-button_danger.i-button_small.remove-yes").click();
        }
    }

}
