package by.oz.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ItemPage extends BasePage {
    public ItemPage(WebDriver driver) {
        super(driver);
    }

    @Step("Add product to shopping cart")
    public void addToShoppingCart() {
        if (isElementPresent(By.cssSelector(".addtocart-btn"))) {
            getElemByCSS(".addtocart-btn").click();
        }
    }

    public void addToFavorites() {

    }
}
