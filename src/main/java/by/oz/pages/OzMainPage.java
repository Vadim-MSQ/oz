package by.oz.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Set;

public class OzMainPage extends BasePage {

    private String pageURL = "https://oz.by/";
    public OzMainPage(WebDriver driver) {
        super(driver);
    }

    @Step("Open main page - OZ.by")
    public void open() {
        driver.get(pageURL);
    }

    //levotsky@gmail.com
    //2UN7fK
    @Step("Sign in to the system with login: {name} and password: {pass}")
    public void loginWithEmail(String name, String pass) {
        getElemByCSS(".top-panel__userbar__auth").click();

        String parentWindowHandler = driver.getWindowHandle(); // Store your parent window
        Set<String> handles = driver.getWindowHandles(); // get all window handles
        driver.switchTo().window(handles.iterator().next()); // switch to popup window

        getElemByCSS(".i-nav-tabs__link").click();
        driver.findElement(By.xpath("//input[@type=\"email\"]")).sendKeys(name);
        driver.findElement(By.xpath("//input[@type=\"password\"]")).sendKeys(pass);
        driver.findElement(By.xpath("//button[@form=\"loginForm\"]")).click();
        waitForPageLoad(driver);
    }


}
