package by.oz;

import io.qameta.allure.Step;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.net.NetworkUtils;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import java.net.URL;
import java.util.concurrent.TimeUnit;

public class DriverManager {
    public WebDriver driver;
    private DesiredCapabilities capability;
    private String hubUrl = "";

    @Step("Create browser: {browserName}")
    public WebDriver initDriver(String browserName) {
        String ipAddress = new NetworkUtils().getIp4NonLoopbackAddressOfThisMachine().getHostAddress();
        hubUrl = "http://" + ipAddress + ":4444/wd/hub";
        try {
            driver = new RemoteWebDriver(new URL(hubUrl), setCapabilities(browserName));
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        } catch (Exception e) {
            throw new SkipException("Can't instantiate driver");
        }
        return driver;
    }

    private Capabilities setCapabilities(String browserName) {
        if (browserName.equalsIgnoreCase("firefox")) {
            capability = DesiredCapabilities.firefox();
            capability.setCapability("marionette", true);
        } else if (browserName.equalsIgnoreCase("chrome")) {
            ChromeOptions options = new ChromeOptions();
            capability = DesiredCapabilities.chrome();
            capability.setCapability(ChromeOptions.CAPABILITY, options);
        } else {
            throw new IllegalArgumentException("The Browser type is undefined");
        }
        return capability;
    }
}