package by.oz;

import by.oz.pages.SearchResultPage;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsStringIgnoringCase;
import static org.hamcrest.Matchers.everyItem;

public class SearchTest extends BaseTest {

    private SearchResultPage searchResultPage;

    @Test
    public void searchTest() {
        searchResultPage = topPanel.search("кинг");
        assertThat(searchResultPage.getSearchResults(), everyItem(containsStringIgnoringCase("кинг")));
    }
}
