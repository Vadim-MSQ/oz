package by.oz;

import by.oz.pages.ItemPage;
import by.oz.pages.SearchResultPage;
import by.oz.pages.ShoppingBasketPage;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class ShoppingBasketTest extends BaseTest {
    private SearchResultPage resultPage;
    private ShoppingBasketPage shoppingBasketPage;

    @Test
    public void oneItemInBasket() {
        resultPage = topPanel.search("Uno");
        ItemPage itempage = resultPage.chooseItem();
        itempage.addToShoppingCart();

        shoppingBasketPage = topPanel.goToCart();
        shoppingBasketPage.waitForPageLoad(driver);
        assertThat(shoppingBasketPage.getNumberOfItems(), equalTo(topPanel.getNumberOfItems()));
        shoppingBasketPage.setQuantityOfItem("Uno", 8);
        assertThat(shoppingBasketPage.getNumberOfItems(), equalTo(topPanel.getNumberOfItems()));
    }

    @Test()
    public void threeItemsInBasket() {
        resultPage = topPanel.search("Uno");
        ItemPage itempage = resultPage.chooseItem();
        itempage.addToShoppingCart();

        resultPage = topPanel.search("Манчкин");
        itempage = resultPage.chooseItem();
        itempage.addToShoppingCart();

        resultPage = topPanel.search("Каркассон");
        itempage = resultPage.chooseItem();
        itempage.addToShoppingCart();

        shoppingBasketPage = topPanel.goToCart();
        shoppingBasketPage.waitForPageLoad(driver);
        assertThat(shoppingBasketPage.getNumberOfItems(), equalTo(topPanel.getNumberOfItems()));
        shoppingBasketPage.setQuantityOfItem("Манчкин", 2);
        assertThat(shoppingBasketPage.getNumberOfItems(), equalTo(topPanel.getNumberOfItems()));
    }

    @Test(dependsOnMethods = "threeItemsInBasket")
    public void deleteAllTest() {
        shoppingBasketPage.checkAllItems();
        shoppingBasketPage.delete();
        assertThat(shoppingBasketPage.getNumberOfItems(), equalTo(topPanel.getNumberOfItems()));
    }

}
