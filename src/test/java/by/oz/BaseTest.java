package by.oz;

import by.oz.pages.OzMainPage;
import by.oz.pages.TopPanel;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

public class BaseTest {
    public WebDriver driver;
    public OzMainPage mainPage;
    public TopPanel topPanel;

    @Parameters({"browser"})
    @BeforeClass
    public void setUp(String browser) {
        driver = new DriverManager().initDriver(browser);
        mainPage = new OzMainPage(driver);
        mainPage.open();
        topPanel = new TopPanel(driver);
    }

    @Step("Browser shut down")
    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
