package by.oz;

import by.oz.pages.SearchResultPage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalToIgnoringCase;

public class LoginTest extends BaseTest {

    @Step("Verify that page title is - '{pageTitle}'")
    @Parameters({"pageTitle"})
    @Test(alwaysRun = true)
    public void verifyMainPageTitle(String pageTitle) {
        assertThat(mainPage.getTitle(),
                equalToIgnoringCase(pageTitle));
    }

    @Test(description = "Sign in to the site via email")
    public void loginToSite() {
        mainPage.loginWithEmail("levotsky@gmail.com", "2UN7fK");
        Assert.assertTrue(mainPage.isElementPresent(By.cssSelector(".top-panel__userbar__user__name__inner")));
    }

    @Test(description = "just to watch that page scrolling up&down")
    public void tuda_suda() throws InterruptedException {
        mainPage.scrollPageDown();
        Thread.sleep(2000);
        mainPage.scrollPageUp();
        Thread.sleep(2000);
    }
}
